import React from 'react';

const Header = ({...props}) =>
    <div>
        <h1>
            TIENDA PESCA
        </h1>
        <h4>
            Empresa MIedoSoftInteractive inc. RUC 9876543210
        </h4>
    </div>

export default Header;