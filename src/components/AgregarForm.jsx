import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';


const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});
/*
const productos = [
  {
    value: '1',
    label: 'Latas de Sardina - S/. 12',
  },
  {
    value: '2',
    label: 'Latas de Lomito	- S/. 12.2',
  },
];
*/
class TextFields extends Component {
  constructor(...props) {
    super(...props);

    this.state = {
      cantidad: '1',
      borrar: '',
      producto: 2,
  };
  }
  

  handleChange = name => event => {
    console.log(name)
    console.log(event.target.value)
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const { classes, productos } = this.props;
    console.log(productos)
    console.log('productos')
    return (
      <form className={classes.container} noValidate autoComplete="off">
        <div>
            <TextField
            id="standard-select-currency"
            select
            label=" "
            className={classes.textField}
            value={this.state.producto}
            onChange={this.handleChange('producto')}
            SelectProps={{
                MenuProps: {
                className: classes.menu,
                },
            }}
            helperText="Selecione Producto"
            margin="normal"
            >
            {productos.map(option => (
                <MenuItem key={option.idproducto} value={option.idproducto}>
                {option.nombre}
                </MenuItem>
            ))}
            </TextField>
        </div>
        <TextField
          id="standard-number"
          label="Cantidad"
          value={this.state.cantidad}
          onChange={this.handleChange('cantidad')}
          type="number"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          margin="normal"
        />
        <Button variant="outlined" color="primary" className={classes.button} onClick={() => this.props.agregarCarro(this.state.producto,this.state.cantidad)}>
        Agregar
        </Button>
        <TextField
          id="standard-number"
          label="Borrar Por Indice"
          value={this.state.borrar}
          onChange={this.handleChange('borrar')}
          type="number"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          margin="normal"
        />
        <Button variant="outlined" color="primary" className={classes.button}>
        Borrar
        </Button>

        <Button variant="outlined" color="primary" className={classes.button}>
        Vaciar
        </Button>
        <Button variant="outlined" color="primary" className={classes.button}>
        Proceder
        </Button>
      </form>
    );
  }
}

TextFields.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TextFields);