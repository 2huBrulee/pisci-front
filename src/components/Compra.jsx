import React, { Component } from 'react';
import { Paper, Grid } from '@material-ui/core';
import Cart from './Cart';
import AgregarForm from './AgregarForm';
import axios from 'axios';

class Compra extends Component {
    constructor(...props) {
      super(...props);
  
      this.state = {
        usuarioActual: 0,
        carrito: []
      }
    }

    agregarCarro = (producto_id, cant) => {
        let producto = {}
        this.props.productos.map(
            (val,i)=> (val.idproducto===producto_id)?producto=val:null
        )
        producto = {...producto,cant}        
        this.setState({
            carrito: [...this.state.carrito,producto]
        })
        

    }
  
    componentWillMount() {
    }
  
    render() {
      const {carrito} = this.state;
      const {productos} = this.props;
      console.log(this.state.carrito)
      return (
        <Grid container spacing={24}>
            <Grid item xs={12} sm={6}>
                <Paper>
                    <Cart carrito={carrito} />
                </Paper>
            </Grid>
            <Grid item xs={12} sm={6}>
                <Paper>
                    <AgregarForm productos={productos} agregarCarro={this.agregarCarro} />
                </Paper>
            </Grid>
        </Grid>
      );
    }
  }
   

export default Compra;