import React, { Component } from 'react';
import Header from './Header';
import './App.css';
import Compra from './Compra';
import Recibo from './Recibo';
import Registro from './Registro';
import axios from 'axios';

class App extends Component {
  constructor(...props) {
    super(...props);

    this.state = {
      usuarioActual: 0,
      clientes: [],
      productos: [],
      vista: 'C',
      venta: []
    }
  }

  componentWillMount() {
    axios.get(`http://localhost:8080/usuarios`)
    .then(res => {
      console.log(res.data)
      this.setState({clientes:res.data})
    })
    .then(
      axios.get(`http://localhost:8080/productos`)
      .then(resp => {
        console.log(resp.data)
        this.setState({productos:resp.data})
      }))
  }

  render() {
    const {vista, productos} = this.state;
    console.log(this.state)
    return (
      <div className="App">
        <Header/>
        {(vista === 'L') ? <Registro/>: <div/> }
        {(vista === 'C') ? <Compra productos={productos}/>: <div/> }
        {(vista === 'R') ? <Recibo/>: <div/> }        
      </div>
    );
  }
}

export default App;
