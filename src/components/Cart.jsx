import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import axios from 'axios';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
      },
      table: {
        minWidth: 700,
      },
    });

let id = 0;
function createData(nombre, cantidad, precio ) {
      id += 1;
      return { id, nombre, cantidad, precio };
    }

let rows = [
        createData('Latas de Sardina', 40, 12.0),
        createData('Latas de Lomito', 12, 12.2),
        createData('10kg Harina de Pescado', 15, 53.0),
        createData('Latas de Caballa', 32.2, 69.0),
        createData('Salchicha de Pescado', 322, 16.0),
      ];

let axioma = () => {
    axios.get('http://localhost:8080/productos')
    .then(res => {
        console.log(res.data)
        let new_rows = [...res.data]
        rows = new_rows
    })
}

let Cart = ({carrito=[],...props}) => {
    
    return (
      <div>
          <Paper>
            <Table>
            <TableHead>
                <TableRow>
                    <TableCell numeric>#
                    </TableCell>
                    <TableCell>Nombres</TableCell>
                    <TableCell numeric>Cantidad</TableCell>
                    <TableCell numeric>Precio</TableCell>
                </TableRow>
            </TableHead>
                <TableBody>
                {carrito.map(row => {
                    return (
                    <TableRow key={row.id}>
                        <TableCell component="th" scope="row" numeric>
                        {row.idproducto}
                        </TableCell>
                        <TableCell >{row.nombre}</TableCell>
                        <TableCell numeric>{row.cant}</TableCell>
                        <TableCell numeric>{row.precio}</TableCell>
                    </TableRow>
                    );
                })}
                </TableBody>
            </Table>
          </Paper>
      </div>
    );
  }
  
Cart.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(Cart);