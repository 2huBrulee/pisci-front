import React from 'react';
import Cart from './Cart';
import { Paper, Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';

const Recibo = (...props) =>
    <Grid container spacing={24}>
        <Grid item xs={6} sm={3}>
        </Grid>
        <Grid item xs={12} sm={6}>
            <div>
                <h3>
                    Compra Realizada con Exito
                </h3>
                <Cart/>
                <Button variant="outlined" color="primary" >
                    Regresar
                </Button>
            </div>
        </Grid>
        <Grid item xs={6} sm={3}>
        </Grid>
    </Grid>
    

export default Recibo;